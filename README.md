# README #

This project will help you run a NodeJS repository as a container

### What is this repository for? ###

* When you have a NodeJS repository you'd want to run in a container
* You use MongoDB as your database
* You have limited to no experience with docker

### How do I get set up? ###

* Change the repo env in the docker file to the repo you'd want to use
* Run `docker-compose build node`
* Run `docker-compose up`

### How do I connect to the MongoDB database? ###
```
const Mongo = require('mongodb').MongoClient;
let MongoConnectionString;
const MongoDB = process.env.MONGO_DATABASE || 'test';
    MongoConnectionString = process.env.MONGO_PORT.replace('tcp','mongodb') + '/' + MongoDB;

Mongo.connect(MongoConnectionString, function (err, db) {
  // Use the db here..
}
```